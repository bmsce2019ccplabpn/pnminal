#include<stdio.h>
#include<math.h>
struct fraction
{
  int num;
  int denom;
};
typedef struct fraction fract;
fract
input ()
{
  fract f;
  printf ("enter the numerator\n");
  scanf ("%d", &f.num);
  printf ("enter the denominator\n");
  scanf ("%d", &f.denom);
  return f;
}

int
numerator (fract f1, fract f2)
{
  int n;
  n = f1.num * f2.denom + f2.num * f1.denom;
  return n;
}

int
denominator (fract f1, fract f2)
{
  int d;
  d = f1.denom * f2.denom;
  return d;
}


void
output (int n, int d)
{
  printf ("the sum of the two fractions is %d/%d", n, d);
}

int
main ()
{
  fract f1, f2;
  int n, d, sum;
  f1 = input ();
  f2 = input ();
  n = numerator (f1, f2);
  d = denominator (f1, f2);
  output (n, d);
  return 0;
}
