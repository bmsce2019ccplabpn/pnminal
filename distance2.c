#include<stdio.h>
#include<math.h>
int input()
{
    float x1;
    printf("enter a coordinate\n");
    scanf("%f",&x1);
    return x1;
}
int compute(float x1,float x2,float y1,float y2)
{
    float distance;
    distance=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    return distance;
}
void output(float distance)
{
    printf("the distance is %f",distance);
}
int main()
{
    float x1,y1,x2,y2,distance;
    x1=input();
    y1=input();
    x2=input();
    y2=input();
    distance=compute(x1,y1,x2,y2);
    output(distance);
    return 0;
}