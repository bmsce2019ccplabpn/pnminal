#include<stdio.h>
#include<math.h>
struct distance
{
    float x;
    float y;
};
typedef struct distance point;
point input()
{
    point p;
    printf("enter the first ordinate\n");
    scanf("%f",&p.x);
    printf("enter the second ordinate\n");
    scanf("%f",&p.y);
    return p;
}
float compute(point p1,point p2)
{
    float distance;
    distance=sqrt((p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y));
    return distance;
}
void output(float distance)
{
    printf("the distance between the points  is %f",distance);
}
int main()
{
    float distance;
    point p1,p2;
    p1=input();
    p2=input();
    distance=compute(p1,p2);
    output(distance);
    return 0;
}
    

