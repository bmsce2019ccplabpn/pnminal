#include <stdio.h>

int
input ()
{
  int n;
  printf ("enter the number of array elements\n");
  scanf ("%d", &n);
  return n;
}

void
read (int n, int a[n])
{
  printf ("enter the array elements\n");
  for (int i = 0; i < n; i++)
    {
      scanf ("%d", &a[i]);
    }
}

void
display (int n, int a[n])
{
  printf ("the array elements are\n");
  for (int i = 0; i < n; i++)
    {
      printf ("a[%d]=%d\n", i, a[i]);
    }
}

int
compute (int n, int a[n])
{
  int sum = 0;
  for (int i = 0; i < n; i++)
    {
      sum = sum + a[i];
    }
  return sum;
}

void
output (int sum)
{
  printf ("\nthe sum of the array elements is %d", sum);
}

int
main ()
{
  int n = input ();
  int a[n];
  read (n, a);
  display (n, a);
  int sum = compute (n, a);
  output (sum);
  return 0;
}
